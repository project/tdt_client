<?php

/**
 * @file
 * Contains \Drupal\tdt_client\Consumer.
 */

namespace Drupal\tdt_client;

use Drupal\Component\Serialization\Json;
use Drupal\tdt_client\Client;
use Drupal\tdt_client\Config\Config;
use Drupal\tdt_client\Entity\Dataset\Dataset;

class Consumer {

  private $datasets;

  /**
   * $get_definition => Do you want to load the definition details of this dataset
   */
  public function __construct(Config $config, $get_definition = FALSE) {
    $client = new Client($config);

    $datasets = $client->get('api/info?limit=-1');

    foreach ($datasets as $id => $dataset) {
      $this->datasets[$id] = new Dataset($dataset, $client, $get_definition);
    }
  }

  /**
   *
   */
  public function getDatasets() {
    return $this->datasets;
  }

  /**
   * Helper function for migrate plugin, migrate requires arrays
   */
  public function getDatasetsArray() {
    $datasets_obj = $this->datasets;
    $datasets = [];
    foreach ($datasets_obj as $key => $dataset) {
      $datasets[$key] = array('dataset_name' => $key, 'dataset' => $dataset);
    }
    return $datasets;
  }

  /**
   * Get unique fields of all the datasets combined.
   */
  public function getUniqueFields($prefix = '') {
    $fields = [];
    foreach ($this->datasets as $dataset) {
      foreach ($dataset->getFields() as $name => $value) {
        $fields[$prefix . $name] = $name;
      }
    }

    return $fields;
  }
}

?>