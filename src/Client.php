<?php

/**
 * @file
 * Contains \Drupal\tdt_client\Client.
 */

namespace Drupal\tdt_client;

use Drupal\Component\Serialization\Json;
use Drupal\tdt_client\Config\Config;

class Client {

  private $config;

  public function __construct(Config $config) {
    $this->config = $config;
  }

  /**
   * @todo Error handling.
   */
  public function getRaw($path) {
    $http = new \GuzzleHttp\Client();

    // Support both absolute and relative urls.
    if (stripos($path, 'ttp://') === FALSE) {
      $path = $this->config->getEndpoint() . $path;
    }

    $response = $http->get($path, [
      'auth' => [$this->config->getUsername(), $this->config->getPassword()],
    ]);

    return $response->getBody()->getContents();
  }

  public function get($path) {
    $response = $this->getRaw($path);

    return Json::decode($response);
  }
}

?>