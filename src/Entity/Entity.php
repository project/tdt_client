<?php

/**
 * @file
 * Contains Drupal\tdt_client\Entity\Entity.
 */

namespace Drupal\tdt_client\Entity;

class Entity implements EntityInterface {

  protected $id_property;
  protected $name_property;
  protected $description_property;
  protected $properties;

  /**
   *
   */
  public function __construct($properties, $id_property = 'id',
                              $name_property = 'name', $description_property = 'description') {
    $this->properties = $properties;
    $this->id_property = $id_property;
    $this->name_property = $name_property;
    $this->description_property = $description_property;
  }

  /**
   *
   */
  public function getId() {
    return $this->properties[$this->id_property];
  }

  /**
   *
   */
  public function getName() {
    return $this->properties[$this->name_property];
  }

  /**
   *
   */
  public function getDescription() {
    return $this->properties[$this->description_property];
  }

  /**
   * Test
   */
  public function getProperty($property) {
    if (isset($this->properties[$property])) {
      return $this->properties[$property];
    }
    return FALSE;
  }

  /**
   * Get All properties
   */
  public function getProperties() {
    return $this->properties;
  }

  /**
   *
   */
  public function setProperties($properties) {
    $this->properties = $properties;

    return $this->$properties;
  }

  /**
   *
   */
  public function setProperty($property, $value) {
    $this->properties[$property] = $value;

    return $this->properties[$property];
  }
}

?>