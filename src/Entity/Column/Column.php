<?php

/**
 * @file
 * Contains Drupal\tdt_client\Entity\Column\Column.
 */

namespace Drupal\tdt_client\Entity\Column;

use Drupal\tdt_client\Entity\ColumnInterface;
use Drupal\tdt_client\Entity\Entity;

class Column extends Entity implements ColumnInterface {

  /**
   *
   */
  public function __construct($properties) {
    parent::__construct($properties, 'column_name', 'column_name_alias', 'description_en');
  }

  /**
   *
   */
  public function getColumnName() {
    return $this->getId();
  }

  /**
   *
   */
  public function getDocumentation() {
    return $this->getDescription();
  }

}

?>