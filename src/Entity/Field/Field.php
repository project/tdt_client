<?php

/**
 * @file
 * Contains Drupal\tdt_client\Entity\Field\Field.
 */

namespace Drupal\tdt_client\Entity\Field;

use Drupal\tdt_client\Entity\FieldInterface;
use Drupal\tdt_client\Entity\Entity;

class Field implements FieldInterface {

  protected $name, $value;

  /**
   *
   */
  public function __construct($name, $value) {
    $this->name = $name;
    $this->value = $value;
  }

  public function getName() {
    return $this->name;
  }

  public function getValue() {
    return $this->value;
  }

}

?>