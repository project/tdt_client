<?php

/**
 * @file
 * Contains Drupal\tdt_client\Entity\Parameter\Parameter.
 */

namespace Drupal\tdt_client\Entity\Parameter;

use Drupal\tdt_client\Entity\Entity;
use Drupal\tdt_client\Entity\ParameterInterface;

class Parameter extends Entity implements ParameterInterface {

  /**
   *
   */
  public function __construct($properties) {
    parent::__construct($properties);
  }

  /**
   *
   */
  public function isRequired() {
    return $this->getProperty('required');
  }

  /**
   *
   */
  public function getDefaultValue() {
    return $this->getProperty('default_value');
  }

  /**
   * Get documentation in english
   */
  public function getDocumentation() {
    if ($this->getProperty('description')) {
      return $this->getProperty('description');
    }
    else {
      return $this->getProperty('description_en');
    }
  }
}

?>