<?php

/**
 * @file
 * Contains Drupal\tdt_client\Entity\Dataset\Dataset.
 */

namespace Drupal\tdt_client\Entity\Dataset;

use Drupal\tdt_client\Entity\DatasetInterface;
use Drupal\tdt_client\Entity\Column\Column;
use Drupal\tdt_client\Entity\Field\Field;
use Drupal\tdt_client\Entity\Entity;
use Drupal\tdt_client\Entity\Parameter\Parameter;
use Drupal\tdt_client\Client;

class Dataset extends Entity implements DatasetInterface {

  protected $client;
  protected $original_dataset;

  /**
   *
   */
  public function __construct($dataset, Client $client, $get_definition = FALSE) {
    $this->client = $client;
    $this->original_dataset = $dataset;

    if ($get_definition) {
      // Add the defintion info to the dataset.
      $raw_properties = array_merge($dataset, $client->get('api/definitions/' . $dataset['identifier']));
    }
    else {
      $raw_properties = $dataset;
    }

    $properties = $this->build($raw_properties);
    parent::__construct($properties, 'identifier', 'title_en', 'userdocumentation_en');
  }

  /**
   * Build the columns, parameters and fields;
   */
  public function build($raw_properties) {
    $properties = [];

    // Make Column objects from columns.
    $columns = [];
    if (isset($raw_properties['columns'])) {
      foreach ($raw_properties['columns'] as $column) {
        $columns[$column['column_name']] = new Column($column);
      }
      unset($raw_properties['columns']);
    }
    $properties['columns'] = $columns;

    // Make Parameter objects from parameters.
    $parameters = [];
    if (isset($raw_properties['parameters'])) {
      foreach ($raw_properties['parameters'] as $id => $parameter) {
        $parameter['id'] = $id;
        $parameter['name'] = $id;
        $parameters[$id] = new Parameter($parameter);
      }
      unset($raw_properties['parameters']);
    }
    $properties['parameters'] = $parameters;

    // Fields
    $fields = [];
    foreach ($raw_properties as $name => $value) {
      $fields[$name] = new Field($name, $value);
    }

    $properties['fields'] = $fields;

    return $properties;
  }

  /**
   * Merge definition info in dataset object
   */
  public function merge_definition() {
    $this->__construct($this->original_dataset, $this->client, TRUE);
  }

  /**
   *
   */
  public function getParameter($parameter) {
    $parameters = $this->getParameters();

    return $parameters[$parameter];
  }

  /**
   *
   */
  public function getParameters() {
    return $this->getProperty('parameters');
  }

  /**
   *
   */
  public function getColumn($column_name) {
    $columns = $this->getColumns();

    return $columns[$column_name];
  }

  /**
   *
   */
  public function getColumns() {
    return $this->getProperty('columns');
  }

  /**
   * Helper function for migrate module, it requires arrays.
   */
  public function getColumnsArray() {
    $columns_arr = $this->getProperty('columns');
    $columns = array();
    foreach ($columns_arr as $key => $column_obj) {
      $columns[$key] = [
        'column_name' => $column_obj->getProperty('column_name'),
        'column_name_alias' => $column_obj->getProperty('column_name_alias'),
        'documentation' => $column_obj->getProperty('documentation')
      ];
    }

    return $columns;
  }

  /**
   * Helper function for migrate module, it requires arrays.
   */
  public function getParameterArray() {
    $param_arr = $this->getProperty('parameters');
    $parameters = array();
    foreach ($param_arr as $key => $param_object) {
      $parameters[$key] = [
        'param_name' => $key,
        'required' => $param_object->isRequired(),
        'documentation' => $param_object->getDocumentation(),
        'documentation_nl' => $param_object->getProperty('description_nl'),
        'default_value' => $param_object->getDefaultValue(),
      ];
    }

    return $parameters;
  }

  /**
   *
   */
  public function getFields() {
    return $this->getProperty('fields');
  }
}

?>