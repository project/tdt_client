<?php

/**
 * @file
 * Contains \Drupal\tdt_client\Config\Config.
 */

namespace Drupal\tdt_client\Config;

class Config {

  protected $endpoint;
  protected $username;
  protected $password;

  /**
   *
   */
  public function __construct($endpoint, $username, $password) {
    $this->endpoint = $endpoint;
    $this->username = $username;
    $this->password = $password;
  }

  /**
   *
   */
  public function getEndpoint() {
    return $this->endpoint;
  }

  /**
   *
   */
  public function getPassword() {
    return $this->password;
  }

  /**
   *
   */
  public function getUsername() {
    return $this->username;
  }

  /**
   *
   */
  public function setEndpoint($endpoint) {
    $this->endpoint = $endpoint;
  }

  /**
   *
   */
  public function setPassword($password) {
    $this->password = $password;
  }

  /**
   *
   */
  public function setUsername($username) {
    $this->username = $username;
  }
}
