<?php

/**
 * @file
 * Contains \Drupal\tdt_client\Config\DrupalConfig.
 */

namespace Drupal\tdt_client\Config;

class DrupalConfig extends Config {

  /**
   *
   */
  public function __construct() {
    $config = \Drupal::config('tdt_client.settings');

    parent::__construct($config->get('endpoint'), $config->get('username'), $config->get('password'));
  }

}
