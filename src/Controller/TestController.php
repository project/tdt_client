<?php

/**
 * @file
 * Contains \Drupal\tdt_client\Controller\TestController.
 */

namespace Drupal\tdt_client\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\tdt_client\Consumer;
use Drupal\tdt_client\Config\DrupalConfig;

class TestController extends ControllerBase {
  public function content() {
    $config = new DrupalConfig();
    $consumer = new Consumer($config);

    $datasets = $consumer->getDatasets();

    $dataset = reset($datasets);
    dpm($dataset->getDescription(), $dataset->getName() . ' (' . $dataset->getId() . ')');
    foreach ($dataset->getColumns() as $column) {
      dpm($column->getDescription(), $column->getName() . ' (' . $column->getId() . ')');
    }
    foreach ($dataset->getParameters() as $parameter) {
      dpm($parameter->getDefaultValue(), $parameter->getName() . ' (' . $parameter->getId() . ')');
    }

    return ['#type' => 'markup', '#markup' => ''];
  }
}

?>